const mongoose = require("mongoose");
const {Schema} = mongoose;

const taskSchema = new Schema({
    tittle:{type:String,required:true},
    description:{type:String,required:true}
})

module.exports = mongoose.model("task",taskSchema);